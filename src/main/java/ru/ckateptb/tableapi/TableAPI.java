package ru.ckateptb.tableapi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import ru.ckateptb.tableapi.menu.inventory.InventoryListener;

public final class TableAPI extends JavaPlugin {

    private static TableAPI instance;
    public InventoryListener inventoryListener;

    public static TableAPI getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        Bukkit.getPluginManager().registerEvents(inventoryListener = new InventoryListener(this), this);
    }

    @Override
    public void onDisable() {
    }
}
