package ru.ckateptb.tableapi.menu;

import org.bukkit.entity.HumanEntity;

/**
 * Core MenuBuilder class
 * @param <T> Type of the builder
 */
public abstract class MenuBuilder<T> {

    public MenuBuilder() {

    }

    /**
     * Shows the Menu to the viewers
     */
    public abstract MenuBuilder show(HumanEntity... viewers);

    /**
     * Refreshes the content of the menu
     */
    public abstract MenuBuilder refreshContent();

    /**
     * Builds the menu
     */
    public abstract <T> T build();

    public abstract void dispose();

}
